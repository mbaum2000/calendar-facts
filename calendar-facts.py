#!/usr/bin/env python

import random

data = ['Did you know that',
        (['the', ('Fall', 'Spring'), 'Equinox'],
         ['the', ('Winter', 'Summer'), ('Solstice', 'Olympics')],
         ['the', ('Earliest', 'Latest'), ('Sunrise', 'Sunset')],
         ['Daylight', ('Saving', 'Savings'), 'Time'],
         ['Leap', ('Day', 'Year')],
         'Easter',
         ['the', ('Harvest', 'Super', 'Blood'), 'Moon'],
         'Toyota Truck Month',
         'Shark Week',
         ),
        (['happens', ('earlier', 'later', 'at the wrong time'), 'every year'],
         ['drifts out of sync with the', ('Sun',
                                          'Moon',
                                          'Zodiac',
                                          [('Gregorian', 'Mayan', 'Lunar', 'iPhone'), 'Calendar'],
                                          'Atomic Clock in Colorado')],
         ['might', ('not happen', 'happen twice'), 'this year']),
        'because of',
        (['Time Zone Legislation in', ('Indiana', 'Arizona', 'Russia')],
         'a decree by the Pope in the 1500s',
         [('Precession', 'Libration', 'Nutation', 'Libation', 'Eccentricity', 'Obliquity'),
          'of the',
          ('Moon', 'Sun', "Earth's Axis", 'Equator', 'Prime Meridian', [('International Date',
                                                                         'Mason-Dixon'), 'Line'])],
         'Magnetic Field Reversal',
         ['an arbitrary decision by', ('Benjamin Franklin', 'Isaac Newton', 'FDR')]),
        '?', ' ',
        'Apparently',
        ('it causes a predictable increase in car accidents.',
         "that's why we have leap seconds.",
         'scientists are really worried.',
         ['it was even more extreme during the', ('Bronze Age.', 'Ice Age.', 'Cretaceous.', '1990s.')],
         ["there's a proposal to fix it, but it", ('will never happen.',
                                                   'actually makes things worse.',
                                                   'is stalled in Congress.',
                                                   'might be unconstitutional.')],
         "it's getting worse and no one knows why.")]

alt = ['While it may seem like trivia, it',
       ('causes huge headaches for software developers',
        'is taken advantage of by high-speed traders',
        'triggered the 2003 Northeast Blackout',
        'has to be corrected for by GPS satellites',
        'is now recognized as a major cause of World War I'),
       '.']


def word_join(wordlist):
    if type(wordlist) is not list:
        raise TypeError
    string = wordlist[0]
    for word in wordlist[1:]:
        if word not in ['?', '.', ' ']:
            string += ' ' + word
        else:
            string += word
    return string


def choose(selection):
    if type(selection) is str:
        return selection
    elif type(selection) is list:
        return word_join([choose(element) for element in selection])
    elif type(selection) is tuple:
        return choose(random.choice(selection))


if __name__ == '__main__':
    print(choose(data))
    print(choose(alt))
